'use strict';

angular.module('trialomicsWebApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'firebase'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        requireLogin: false
      })
      .when('/dashboard', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl',
        requireLogin: true
      })
      .when('/shared', {
        templateUrl: 'views/shared.html',
        controller: 'SharedCtrl',
        requireLogin: true
      })
      .otherwise({
        redirectTo: '/'
      });
  });
