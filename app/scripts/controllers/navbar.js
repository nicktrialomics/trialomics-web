/* global Firebase */
'use strict';

angular.module('trialomicsWebApp')
  .controller('NavbarCtrl', function ($scope, $firebaseSimpleLogin, $rootScope, $location) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var ref = new Firebase('https://trialomics-web.firebaseio.com/');
    $rootScope.auth = $firebaseSimpleLogin(ref);

    $rootScope.$on('$firebaseSimpleLogin:login', function() {
      $location.path('/dashboard').replace();
      $scope.$apply();
    });

    $rootScope.$on('$firebaseSimpleLogin:logout', function() {
      $location.path('/').replace();
      $scope.$apply();
    });

    


  });
